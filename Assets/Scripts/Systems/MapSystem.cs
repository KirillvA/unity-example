﻿using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    [EcsInject]
    public class MapSystem : IEcsInitSystem, IEcsRunSystem
    {
        EcsWorld _world = null;
        public string MapTag = "Map";
        private Map map;
        public void Initialize()
        {
            _world.CreateEntityWith(out map);
            var _mapMaxBounds = new Vector2Int(100, 100);
            map.mapMaxBounds = _mapMaxBounds;
            map._data = new MapNode[_mapMaxBounds.x, _mapMaxBounds.y];

            for (int x = 0; x < _mapMaxBounds.x; x++)
            {
                for (int y = 0; y < _mapMaxBounds.y; y++)
                {
                    map._data[x, y] = new MapNode(Random.Range(0, 2));
                }
            }
            Debug.Log(map._data);
        }
        public void Run()
        {

        }

        public void Destroy()
        {
            map._data = null;
        }
    }
}
