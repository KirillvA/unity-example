﻿using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    [EcsInject]
    class UserInputSystem : IEcsRunSystem
    {
        EcsWorld _world = null;
        public void Run()
        {
            var mouseClick = Input.anyKey;
            /*var x =  GetAxis("Horizontal");
            Direction direction;
            if (x > 0f)
            {
                direction = Direction.Right;
            }
            else if (x < 0f)
            {
                direction = Direction.Left;
            }
            else
            {
                return;
            }*/
            if (mouseClick)
            {
                _world.CreateEntityWith(out UserInput inputEvent);
                inputEvent.eventType = UserInputType.LeftClick;
                inputEvent.position = Vector2Int.zero;
            }
            //inputEvent.MoveDirection = direction;
        }
    }
}
