﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Systems;
using UnityEngine;

namespace Components
{
    public class Movement
    {
        public Vector2Int target;
        public List<Vector2Int> targetPath;
    }
}
