﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Components
{
    public class MapNode
    {
        public int hardness = 0;

        public MapNode(int hardness)
        {
            this.hardness = hardness;
        }
    }

    public class Map
    {
        public Vector2Int mapMaxBounds;
        public MapNode[,] _data;
        public bool IsCellBlocked(Vector2Int position)
        {
            if (this._data[position.x, position.y].hardness > 0)
                return true;
            else return false;

        }
    }


}
