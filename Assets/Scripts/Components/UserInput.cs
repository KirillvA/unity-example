﻿
using Systems;
using UnityEngine;

namespace Components
{
    public class UserInput
    {
        public UserInputType eventType;
        public Vector2Int position;
    }
}
