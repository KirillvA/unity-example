﻿using Leopotam.Ecs;
using Systems;
using UnityEngine;

public class GameStartup : MonoBehaviour
{
    public float PlayerSpeed;

    EcsWorld _world;
    EcsSystems _systems;

    private void OnEnable()
    {
        _world = new EcsWorld();

#if UNITY_EDITOR
        Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create(_world);
#endif     
        _systems = new EcsSystems(_world)
            .Add(new UserInputSystem())
            .Add(new MapSystem());

        _systems.Initialize();
#if UNITY_EDITOR
        Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_systems);
#endif
    }

    private void Update()
    {
        _systems.Run();
    }

    private void OnDisable()
    {
        _systems.Dispose();
        _world.Dispose();
    }
}